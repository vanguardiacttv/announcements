Bitcoin Cash Node Technical Bulletin
--------------------------
 Date: Apr 25, 2021
 Author: matricz
 
---

## Performance of BCHN v23.0.0 with reworked mempool

BCHN has recently reworked the mepool code, including removing CPFP code (merge requests 1127 through 1130). This allows for accepting unbounded chains of unconfirmed transactions (in preparation of the May'21 upgrade) and improves performance.

A performance test was conducted confronting version v23.0.0 with the previous version v22.2.0. 

## Methodology

The latest Raspberry Pi 4 (8GB of RAM, 64bit Raspbian OS) was used. Series of transactions were generated on the Scalenet network and performance of transaction acceptance and of generating block templates were measured (the blocks weren't actually mined with these templates).

Release versions of binaries were downloaded from the official website. The following configuration options were tweaked: `-blockmaxsize=256000000`, `-maxmempool=1300` and `-maxorphantx=1000000`.

Transactions were generated with the [txunami](https://github.com/gandrewstone/txunami) tool.

## Performance tests

### Accepting transactions to the mempool

Non chained transactions were generated at a rate of 20k tx/s, and periodic readings from `getmempoolinfo` were confronted.

- v22.2.0 accepted transactions at a rate of 1050tx/s
- v23.0.0 accepted transactions at a rate of 1350tx/s for a **28% speedup**

Moreover, if chained transactions were sent, v22.2.0 would progressively slow down acceptance down to a minimum of 500tx/s while approaching the 50 transaction chain limit. v23.0.0 did not present such slowdowns.

### Generating block templates

Mempools of almost one million transactions were generated in two different configurations: 20k chains of length 50 (the worst case possible for v22), and one million independent transactions (the best case for v22). `getblocktemplatelight` was called several times and the reported internal duration is considered (there is some time spent transferring data to the client, which we disregard here).

- In the case of 20k chains of length 50 (these were later mined in block [32734](https://sbch.loping.net/block-height/32734))
  - v22.2.0 took 271 seconds
  - v23.0.0 took 31 seconds for an **88% improvement**
- In the case of 1 million independent transactions (these were later mined in block [32737](https://sbch.loping.net/block-height/32737))
  - v22.2.0 took 36,5 seconds
  - v23.0.0 took 23,5 seconds for a **35% improvement**

## Conclusion

The mempool rework in v23.0.0 provides a very important speedup for working with chained transactions. Even while working with independent transactions, the mempool code rework provides considerable speedup for transaction acceptance and mining. 

## Availability

The official binaries for v23.0.0 are available from BCHN's website. While the transaction processing and mining speedups are available right away, chaining more than 50 transactions will not be possible until the network upgrade on 15th May 2021. 
