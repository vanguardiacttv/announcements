February 20, 2020

Announcement regarding Bitcoin Cash Node

The Bitcoin ABC version 0.21.0 client, released for the May 15th 2020 upgrade, includes controversial changes to BCH coin issuance.
It is generally agreed that the lack of consensus and method of activation significantly raise the risk of a chain split.
Due to strong demand from prominent Bitcoin businesses, miners, mining pools, and developers, we are releasing a minimally-altered client based on Bitcoin ABC.
The only difference between the new client and ABC v0.21.0 is the removal of the coinbase reward diversion code and its associated signaling and activation.

The primary goal of this initiative is to provide a safe and professional node implementation that will neutrally follow the longest chain without contributing to the risk of a chain split.
The client is called Bitcoin Cash Node (BCH Node) and will function as a drop-in replacement for miners and businesses already running the ABC client.
The BCH Node project is led by freetrader, one of the original creators of Bitcoin Cash and Bitcoin ABC.
The project team includes contributors from several other node implementations.

In addition to the release, we commit to maintaining the alternate client for at least one year and cooperating with the wider ecosystem in a transparent process that will help avoid a similar situation in the future.

Our contributors include:

- Andrea Suisani (sickpig) of Bitcoin Unlimited
- Calin Culianu (NilacTheGrim) of Electron Cash, Fulcrum
- Dagur V. Johannsson
- emergent_reasons
- Fernando Pelliccioni of Knuth Node
- freetrader formerly of Bitcoin ABC
- imaginary_username
- Jt Freeman of fountainhead.cash

Developers in support of the effort:
- Jonathan Silverblood
- Josh Green of Bitcoin Verde
- Mark Lundeberg
- Tom Zander of Flowee the Hub

We are in active communication with exchanges, mining pools and miners.
People who want to find out more should contact freetrader via any of his channels (BitcoinCashNode slack, BTCFork slack as backup, Memo, email - ask if you need).

Bitcoin Cash Node software will be available soon, followed by a more detailed near-term plan.

The Bitcoin Cash Node team.